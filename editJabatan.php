<?php include 'header.php';
include 'connect.php';

$id_jabatan = $_GET['id_jabatan'];
$sql = "SELECT * FROM jabatan WHERE id_jabatan='$id_jabatan'";
$query = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_object($query)) {
?>

<h1 class="mt-3 mb-3">Form Edit Jabatan</h1>
<form action="saveEditJabatan.php" method="POST">

    <div class="mb-3">
        <input type="hidden" class="form-control" name="id_jabatan" value = "<?php echo $row->id_jabatan; ?>" required>
    </div>
    <div class="mb-3">
        <label class="form-label">Jabatan</label>
        <input type="text" class="form-control" name="jabatan" value = "<?php echo $row->jabatan; ?>" required>
    </div>
    <div class="mb-3">
        <input type="submit" value="Simpan" class="btn btn-sm btn-success">
    </div>
</form>
<?php } ?>

<?php include 'footer.php'; ?>