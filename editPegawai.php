<?php include 'header.php';
include 'connect.php';

$id_pegawai = $_GET['id_pegawai'];
$sql = "SELECT * FROM pegawai WHERE id_pegawai='$id_pegawai'";
$query = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_object($query)) {
?>

<h1 class="mt-3 mb-3">Form Edit Pegawai</h1>
<form action="saveEditPegawai.php" method="POST">

    <div class="mb-3">
        <input type="hidden" class="form-control" name="id_pegawai" value = "<?php echo $row->id_pegawai; ?>" required>
    </div>
    <div class="mb-3">
        <label class="form-label">Nama</label>
        <input type="text" class="form-control" name="nama" placeholder="Nama" value="<?php echo $row->nama ?>" required>
    </div>
    <div class="mb-3">
        <label class="form-label">Jenis Kelamin</label>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="jenis_kelamin" value="<?php echo $row->jenis_kelamin ?>" id="pria" checked>
            <label class="form-check-label" for="pria">
                Pria
            </label>
            </div>
            <div class="form-check">
            <input class="form-check-input" type="radio" name="jenis_kelamin" value="Wanita" id="wanita">
            <label class="form-check-label" for="wanita">
                Wanita
            </label>
        </div>
    </div>
    <div class="mb-3">
        <label class="form-label">Tanggal Lahir</label>
        <input type="date" class="form-control" name="tanggal_lahir" placeholder="Tanggal Lahir" value="<?php echo $row->tanggal_lahir ?>" required>
    </div>
    <div class="mb-3">
        <label class="form-label">Alamat</label>
        <textarea class="form-control" name="alamat" rows="3" required><?php echo $row->alamat ?></textarea>
    </div>
    <div class="mb-3">
        <label class="form-label">Jabatan</label>
        <select class="form-control" name="id_jabatan"   required>

        <?php
            $sql = 'SELECT * FROM jabatan';
            $query = mysqli_query($conn, $sql);
            while ($row = mysqli_fetch_object($query)) {
        ?>
            <option value="<?php echo $row->id_jabatan; ?>"><?php echo $row->jabatan; ?></option>
        
        <?php } ?>

        </select>
    </div>
    <div class="mb-3">
        <input type="submit" value="Simpan" class="btn btn-sm btn-success">
    </div>
</form>
<?php }// end ?>

<?php include 'footer.php'; ?>